### Описание
```
7.2 Дан несортированный массив чисел. дана сумма какая то n. нужно выяснить есть ли в массиве два числа которые в сумме дают n  
7.3 Дан некий массив строк состоящих из строчных латинских символов, задача найти максимальный общий префикс среди всех строк.  
Пример:  
[ "qwe", "qweasd", "qwsdfsdf", "tr" ] -> ""
[ "qwe", "qweasd", "qwsdfsdf" ] -> "qw"
[ "qwe", "qeasd", "qwsdfsdf" ] -> "q"
7.1 Дан массив чисел. дано расстояние k. если числа в массиве находятся рядом то они на расстоянии 1 друг от друга. нужно найти количество пар дублей, которые находятся на расстоянии k друг от друга
```
### запуск тестов

```
composer install

./vendor/bin/phpunit --bootstrap vendor/autoload.php findNumberOfPairsAtDistance7.1/TestFindNumberOfPairsAtDistance.php
./vendor/bin/phpunit --bootstrap vendor/autoload.php isPairWithGivenSumExists7.2/TestIsPairWithGivenSumExists.php
./vendor/bin/phpunit --bootstrap vendor/autoload.php findCommonPrefix7.3/TestCommonPrefixFinder.php
```