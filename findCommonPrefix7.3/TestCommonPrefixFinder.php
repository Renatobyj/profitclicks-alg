<?php
declare(strict_types = 1);

use PHPUnit\Framework\TestCase;

class TestCommonPrefixFinder extends TestCase
{
    public function testfindCommonPrefix()
    {
        $commonPrefixFinder = new CommonPrefixFinder();

        $arr = [];
        $expected = '';
        $actual = $commonPrefixFinder->findCommonPrefix($arr);

        $this->assertEquals($expected, $actual);

        $arr = [ "qwe", "qweasd", "qwsdfsdf", "tr" ];
        $expected = '';
        $actual = $commonPrefixFinder->findCommonPrefix($arr);

        $this->assertEquals($expected, $actual);

        $arr = [ "qwe", "qweasd", "qwsdfsdf" ];
        $expected = 'qw';
        $actual = $commonPrefixFinder->findCommonPrefix($arr);

        $this->assertEquals($expected, $actual);

        $arr = [ "qwe", "qeasd", "qwsdfsdf" ];
        $expected = 'q';
        $actual = $commonPrefixFinder->findCommonPrefix($arr);

        $this->assertEquals($expected, $actual);

        $arr = [ "qwe", 1];
        $err = null;
        try {
            $commonPrefixFinder->findCommonPrefix($arr);
        } catch (Error $err) {
        }

        $this->assertInstanceOf(Error::class, $err);
    }
}
