<?php
declare(strict_types = 1);

/**
 * 7.3 Дан некий массив строк состоящих из строчных латинских символов, задача найти максимальный общий префикс среди всех строк.
 * Пример:
 * [ "qwe", "qweasd", "qwsdfsdf", "tr" ] -> ""
 * [ "qwe", "qweasd", "qwsdfsdf" ] -> "qw"
 * [ "qwe", "qeasd", "qwsdfsdf" ] -> "q"
 *
 * берем первое значение в качестве префикса
 * затем пробегаемся по всем остальным значениям и ищем длину общего префикса (сравниваем строки посимвольно)
 * в конце отрезаем от префикса полученную длину
 */
class CommonPrefixFinder
{

    
    /**
     * @param string[] $arr
     * @return string
     */
    public function findCommonPrefix(array $arr):string
    {
        if (count($arr) === 0) {
            return '';
        }


        $prefix = $arr[0];
        $length = mb_strlen($prefix);
        for ($i = 1; $i < count($arr); $i++) {
            $value = $arr[$i];
            $commonPrefixLength = $this->findCommonPrefixLength($prefix, $value);
            $prefix = mb_substr($prefix, 0, $commonPrefixLength);
            if ($prefix === '') {
                break;
            }
        }

        return $prefix;
    }

    /**
     * @param string $val1
     * @param string $val2
     * @return int
     */
    private function findCommonPrefixLength(string $val1, string $val2):int
    {
        $length1 = mb_strlen($val1);
        $length2 = mb_strlen($val2);

        $maxPrefixLength = $length1 < $length2 ? $length1 : $length2;
        $prefixLength = 0;

        while ($prefixLength < $maxPrefixLength) {
            if ($val1[$prefixLength] !== $val2[$prefixLength]) {
                break;
            }

            $prefixLength++;
        }

        return $prefixLength;
    }
}
