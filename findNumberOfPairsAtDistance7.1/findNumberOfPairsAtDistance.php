<?php
declare(strict_types = 1);

/**
 * 7.1 Дан массив чисел. дано расстояние k. если числа в массиве находятся рядом то они на расстоянии 1 друг от друга.
 * нужно найти количество пар дублей, которые находятся на расстоянии k друг от друга
 *
 * пробегаемся по массиву и сравниваем текущий элемент с элементом на расстоянии k от текущего
 *
 * @param array $arr
 * @param int $k
 * @return int
 */
function findNumberOfPairsAtDistance(array $arr, int $k): int
{
    if ($k <= 0) {
        throw new Exception('$k must be positive integer > 0');
    }

    $numberOfPairs = 0;
    for ($i = 0; $i < count($arr) - $k; $i++) {
        if ($arr[$i] === $arr[$i + $k]) {
            $numberOfPairs++;
        }
    }

    return $numberOfPairs;
}
