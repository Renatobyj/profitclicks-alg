<?php
declare(strict_types = 1);

use PHPUnit\Framework\TestCase;

class TestFindNumberOfPairsAtDistance extends TestCase
{
    public function test()
    {
        $arr = [];
        $k = 1;
        $expected = 0;
        $actual = findNumberOfPairsAtDistance($arr, $k);
        $this->assertEquals($expected, $actual);

        $arr = [];
        $k = 0;
        $err = null;
        try {
            findNumberOfPairsAtDistance($arr, $k);
        } catch (Exception $err) {
        }
        $this->assertInstanceOf(Exception::class, $err);

        $arr = [2, 66, 45, 90, 10, 45];
        $k = 3;
        $expected = 1;
        $actual = findNumberOfPairsAtDistance($arr, $k);

        $this->assertEquals($expected, $actual);

        $arr = [1, 2, 1, 4, 1, 7, 1, 6];
        $k = 2;
        $expected = 3;
        $actual = findNumberOfPairsAtDistance($arr, $k);

        $this->assertEquals($expected, $actual);
    }
}
