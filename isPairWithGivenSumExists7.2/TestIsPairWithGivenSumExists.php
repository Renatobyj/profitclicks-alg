<?php
declare(strict_types = 1);

use PHPUnit\Framework\TestCase;

class TestIsPairWithGivenSumExists extends TestCase
{
    public function test()
    {
        $arr = [];
        $sum = 0;
        $expected = false;
        $actual = isPairWithGivenSumExists($arr, $sum);

        $this->assertEquals($expected, $actual);

        $arr = [1, 2, 3];
        $sum = 3;
        $expected = true;
        $actual = isPairWithGivenSumExists($arr, $sum);

        $this->assertEquals($expected, $actual);


        $arr = [1, 2, 3];
        $sum = 8;
        $expected = false;
        $actual = isPairWithGivenSumExists($arr, $sum);

        $this->assertEquals($expected, $actual);
    }
}
