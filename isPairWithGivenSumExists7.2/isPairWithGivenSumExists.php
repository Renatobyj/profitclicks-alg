<?php
declare(strict_types = 1);

/**
 * 7.2 Дан несортированный массив чисел. дана сумма какая то n. нужно выяснить есть ли в массиве два числа которые в сумме дают n
 *
 * Заполняем хэш-таблицу значениями, которые мы хотим найти в дальнейшем(разностью между данной суммой и значением в массиве)
 * и за тот же проход проверяем есть ли текущее значение в этой таблице.
 */
function isPairWithGivenSumExists(array $arr, int $sum): bool
{
    $expected = [];
    foreach ($arr as $value) {
        if (array_key_exists($value, $expected)) {
            return true;
        }

        $expectedValue = $sum - $value;
        $expected[$expectedValue] = null;
    }

    return false;
}
